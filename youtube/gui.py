#!/usr/bin/env python
import gtk
import os
import subprocess


class YoutubeDLGui:
    def download(self, widget, data=None):
        p = subprocess.Popen(["youtube-dl", "-e", self.entry.get_text()], stdout=subprocess.PIPE)
        name = p.stdout.readline().strip()
        print self.entry.get_text()
        chooser = gtk.FileChooserDialog(title=None,action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                  buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_SAVE,gtk.RESPONSE_OK))
        chooser.set_default_response(gtk.RESPONSE_OK)
        chooser.set_current_name(name+".flv")
        chooser.run()
        file = chooser.get_filename()
        chooser.destroy()
        p = subprocess.Popen(["youtube-dl", "-o", file, self.entry.get_text()], stdout=subprocess.PIPE)
    
    def delete_event(self, widget, event, data=None):
        return False
    
    def destroy(self, widget, data=None):
        gtk.main_quit()
    
    def __init__(self):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("delete_event", self.delete_event)
        self.window.connect("destroy", self.destroy)
        self.hbox = gtk.HBox()
        self.window.add(self.hbox)
        self.label = gtk.Label("URL: ")
        self.hbox.add(self.label)
        self.entry = gtk.Entry()
        self.entry.connect("activate", self.download, None)
        self.hbox.add(self.entry)
        self.button = gtk.Button("Download")
        self.button.connect("clicked", self.download, None)
        self.hbox.add(self.button)
        self.window.show_all()
        
    def main(self):
        gtk.main()

if __name__ == "__main__":
    youtubedlgui = YoutubeDLGui()
    youtubedlgui.main()