from plings_output import Plings
import config
import re

plings = Plings(config.apikey, True)
#activities = plings.get_activities(days=6, MaxResults=50)
activities = plings.get_activities(file="tmp.xml")

for activity in activities:
    m = re.search("<[^<>]+>", activity["Details"])
    if m:
        print activity["Starts"]
        print activity["id"]
        print m.group(0)
        print