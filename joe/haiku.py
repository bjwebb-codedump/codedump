# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
# Copyright (c) 2010 Joe Henthorn
# Released as free software under the MIT license,
# see the LICENSE file for details.

import random

vocab = {
    "your": 1,
    "mum": 1,
    "is": 1,
    "so": 1,
    "very": 2,
    "fat": 1,
    "inconspicuous": 5,
    "overnumerousness":6,
    "vituperate":4,
    "verbose":2,
    "large":1,
    "womanly":3,
    "full of beans":3,
    "like a bear":3,
    "under a rock":3,
    "midday sun":3,
    "your mum":2,
    "tnnetenba":4,
    "bitfolk support":4,
    "orgcon":2,
    "cory doctorow":5,
    "tom watson":3,
    "fez":1,
    "sock":1,
    "pancakes":2,
    "whimsy":2,
    "full of":2
}

def line(num):
    while True:
        word, syllables = random.choice(vocab.items())
        if num == 0: return ""
        elif syllables <= num:
            return word + " " + line(num-syllables)

print line(5)
print line(7)
print line(5)

"""
large sock pancakes your 
midday sun under a rock fat 
bitfolk support sock 
[bjwebb@webbpad joe]$ python haiku.py 
full of beans your mum 
fat bitfolk support whimsy 
orgcon is fez sock 

"""
