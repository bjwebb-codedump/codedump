import json, urllib2, os

searchurl = "http://epgservices.sky.com/tvlistings-proxy/TVListingsProxy/freeTextSearch.json?query=How+Clean+Is+Your+House%3F&fromSuggestion=true&genres=&siteId=1"

query = json.load(urllib2.urlopen(searchurl))
for result in query["results"]["result"]:
    for screening in result["screenings"]["screening"]:
        progurl = "http://epgservices.sky.com/tvlistings-proxy/TVListingsProxy/programmeDetails.json?channelId="+screening["channelid"]+"&eventId="+screening["eventid"]+"&siteId=1"
        html = urllib2.urlopen(progurl).read()
        f = open(os.path.join("cache", screening["channelid"]+"-"+screening["eventid"]+".json"), "w")
        f.write(html)
        f.close()