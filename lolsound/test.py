import urllib2
import os, re

words = "a".split(" ")
for word in words:
    if not word+".wav" in os.listdir("."):
        if word == "us": topic = "us-1"
        elif word == "is": topic = "is-2"
        else: topic = word
        html = urllib2.urlopen("http://www.answers.com/topic/"+topic).read()
        url = re.search("'([^']*\.wav)'", html).group(1)
        out = open(word+".wav","w")
        out.write(urllib2.urlopen(url).read())
        out.close()
print "mplayer "+" ".join(map(lambda x: x+".wav", words))

