import couchdb
import re

class Activities():
    db = None
    
    def __init__(self, db):
        self.db = db
    
    def __iter__(self):
        db = self.db
        act = re.compile("a[0-9]*")
        for id in db:
            if act.match(id):
                activity = db[id]
                activity["venue"] = db[activity["venue"]]
                activity["provider"] = db[activity["provider"]]
                yield activity

class Plings():
    couch = None
    db = None
    
    def __init__(self, dbname="plings"):
        self.couch = couchdb.Server()
        self.db = self.couch[dbname]
    
    def get_activities(self):
        return Activities(self.db)
    
    


