from plings_couchdb import Plings

class HelloWorld(object):
    def __init__(self):
        self.plings = Plings()
    
    def index(self):
        out = ""
        activities = self.plings.get_activities()
        for activity in activities:
            print activity
            break
            out += "<ul>"
            for k,v in activity.items():
                if v.__class__.__name__ == "str":
                    out += "<li><strong>"+k+"</strong>"+v+"</li>"
            out += "</ul>"
            out += "<br/><br/>"
            break
        return out
    
    index.exposed = True

h = HelloWorld()
print h.index()