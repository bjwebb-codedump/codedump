from plings_output import Plings
import config
import couchdb

couch = couchdb.Server()
if "plings" in couch:
    db = couch["plings"]
else:
    db = couch.create("plings")

plings = Plings(config.apikey, True)
activities = plings.get_activities(days=6, la="00BW")

for activity in activities:
    try:
        if "a"+activity["id"] in db:
            db.delete(db["a"+activity["id"]])
        if "v"+activity["venue"]["id"] in db:
            db.delete(db["v"+activity["venue"]["id"]])
        if "p"+activity["provider"]["id"] in db:
            db.delete(db["p"+activity["provider"]["id"]])
        db["v"+activity["venue"]["id"]] = activity["venue"]
        activity["venue"] = "v"+activity["venue"]["id"]
        db["p"+activity["provider"]["id"]] = activity["provider"]
        activity["provider"] = "p"+activity["provider"]["id"]
        db["a"+activity["id"]] = activity
    except KeyError: pass