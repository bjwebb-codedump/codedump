#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb67@googlemail.com>
import urllib2
from xml.etree.ElementTree import iterparse, tostring
import copy

def node_to_dict(node):
    if len(node) == 0: return node.text
    data = copy.copy(node.attrib)
    for child in node:
        childdata = node_to_dict(child)
        #if childdata.__class__.__name__ == "str" or childdata.__class__.__name__ == "NoneType":
        if not (child.tag == "keyword" or child.tag == "category"):
            data[child.tag] = childdata
        else:
            if not data.has_key(child.tag):
                data[child.tag] = []
            data[child.tag].append(childdata)
    return data

class Activities():
    itp = None
    root = None
    
    def __init__(self, f):
        self.itp = iter(iterparse(f, events=['start', 'end']))
    
    def __iter__(self):
        while True:
            event, node = self.itp.next()
            if event == "start":
                if node.tag == "activities":
                    root = node
            elif event == "end":
                if node.tag == "activity":
                    #print node.getchildren()
                    tmp = node_to_dict(node)
                    yield tmp
                    #root.remove(node)

class Plings():
    apikey = None
    
    def __init__(self, apikey=None, live=True):
        if live:
            self.apikey = apikey + "-L"
        else:
            self.apikey = apikey
    
    def get_activities(self, days=0, o=None, la=None, MaxResults=None, searchDate=None):
        #return Activities(open("loadsaxml.xml"))
        if o: filters = "/o/"+str(o)
        elif la: filters = "/la/"+la
        params = ""
        if MaxResults: params += "&MaxResults=" + str(MaxResults)
        if searchDate: params += "&searchDate=" + searchDate
        feed = "http://feeds.plings.net/xml.activity.php/"+str(days)+filters+"?APIKey="+self.apikey+params
        print feed
        f = urllib2.urlopen(feed)
        return Activities(f)

