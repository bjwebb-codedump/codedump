import sys
from collections import deque

cells = [ 0 ]
pointer = 0
loops = []
loop = -1
skip = -1

file = open(sys.argv[1], 'r')
while True:
    if loop == -1:
        byte = file.read(1)
    else:
        if skip != -1:
            byte = file.read(1)
            if byte == '[':
                loop += 1
            elif byte == ']':
                if skip == loop:
                    skip = -1
                else:
                    loop -= 1
            continue
        elif loops[loop][0] == 'First':
            byte = file.read(1)
        else:
            byte = loops[loop].popleft()
        loops[loop].append(byte)
    if len(byte) == 0: break
    elif byte == '>':
        pointer += 1
        if pointer >= len(cells): cells.append(0)
    elif byte == '<': pointer -= 1
    elif byte == '+': cells[pointer] += 1
    elif byte == '-': cells[pointer] -= 1
    elif byte == '.': sys.stdout.write(chr(cells[pointer]))
    elif byte == ',': cells[pointer] = ord(sys.stdin.read(1))
    elif byte == '[':
        if cells[pointer] == 0:
            skip = loop
        else:
            loops.append(deque(['First']))
            loop = len(loops)-1
    elif byte == ']':
        if cells[pointer] == 0:
            del(loops[loop])
            loop -= 1
        elif loops[loop][0] == 'First':
            loops[loop].popleft()
    
print
file.close()