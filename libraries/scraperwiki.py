import urllib2
import mechanize

br = mechanize.Browser()
br.set_handle_robots(False)

def scrape(url):
    return br.open(url).read()