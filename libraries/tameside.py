import scraperwiki
import re
import urllib
from BeautifulSoup import BeautifulSoup

# retrieve a page
baseurl = "http://intouch.tameside.gov.uk/vubis/"
starting_url = baseurl + "ListBody.csp?"+\
"SearchMethod=Find_1&Profile=Default&OpacLanguage=eng&EncodedRequest="+\
"*3A*86X*14*7F*9D*E0*C6*26*2F3*2E*ACb*FA*97&PageType=RecordListFind&WebPageNr=1&RowRepeat=0"

html = scraperwiki.scrape(starting_url)
#print html
soup = BeautifulSoup(html)

# use BeautifulSoup to get all <td> tags
tds = soup.findAll("td", attrs={"class": re.compile("listitem(Odd|Even)") }) 
oldurl = ""
for td in tds:
    if td.find("a"):
        url = unicode(td.find("a")["href"])
        if url == oldurl:
            continue
        oldurl = url
        #print baseurl + url
        html2 = scraperwiki.scrape(baseurl + url.replace(" ", "%20"))
        #print html2
        m = re.search("FullBBBody.csp\?[^\"]+", html2)
        if m:
            url2 = m.group(0)
            #print baseurl + url2
            
            #m = re.search("EncodedRequest=([^&]+)", url2)
            #encoded_request = m.group(1)
            #newurl = baseurl + "FullBBBody.csp?"+\
            #    "SearchMethod=Find_1&Profile=Default&OpacLanguage=eng&EncodedRequest="+\
            #    encoded_request
            #print newurl
            
            html3 = scraperwiki.scrape( baseurl + url2 )            
            #html3 = scraperwiki.scrape( newurl )
            page = BeautifulSoup(html3)
            full_desc = page.find("table", summary="FullBB.Description")
            for td in full_desc.findAll("td", "descrname"):
                try:
                    print td.contents[0].strip(),
                    print td.parent.find("td", "descrdata").contents[0].strip()
                except AttributeError:
                    pass
        print
        #record = { "td" : td.text }
        # save records to the datastore
        #scraperwiki.datastore.save(["td"], record) 
    